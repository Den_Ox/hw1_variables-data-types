/* 
1. Як можна оголосити змінну у Javascript?
Огласить переменную можно с помощью ключевых слов "var", "let" & "const".
2. У чому різниця між функцією prompt та функцією confirm?
Разница между promt & confirm в том, что promt возвращает введенный текст в модальном окне или null, а confirm возвращает true или false в зависимости от ответа пользователя
3. Що таке неявне перетворення типів? Наведіть один приклад.
Неявное преобразование типов - это особенность языка JS, когда он самостоятельно (не принудительно) определяет тип полученных данных. Например: console.log (5 * '5'), результат - число 25. Но console.log (5 * 'пять'), результат - NaN
*/

// 1. Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.
let admin = 'Denys';
let name;
name = 'Denys';
console.log(admin);

// 2. Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.
let days = 2;
console.log(days * 3660);

//3. Запитайте у користувача якесь значення і виведіть його в консоль.

console.log(prompt('Write something'));
